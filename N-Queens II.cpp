class Solution {
public:
    int totalNQueens(int n) {
        int total = 0;
        vector<bool> c(n, false);
        vector<bool> d1(n + n - 1, false);
        vector<bool> d2(n + n - 1, false);
        search(0, n, c, d1, d2, total);
        return total;
    }
    
private:
    void search(int i, const int n, vector<bool> &c, vector<bool> &d1, vector<bool> &d2, int &total) {
        if (i == n) {
            total++;
        }
        else {
            for (int j = 0; j < n; j++) {
                if (c[j] || d1[i - j + n - 1] || d2[i + j]) continue;
                c[j] = d1[i - j + n - 1] = d2[i + j] = true;
                search(i + 1, n, c, d1, d2, total);
                c[j] = d1[i - j + n - 1] = d2[i + j] = false;
            }
        }
    }
};
