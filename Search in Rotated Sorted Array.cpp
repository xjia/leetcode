class Solution {
public:
    int search(int A[], int n, int target) {
        int boundary = firstRightEnd(A, n);
        int index = binarySearch(A, 0, boundary, target);
        if (index == -1)
            return binarySearch(A, boundary + 1, n - 1, target);
        return index;
    }
    
private:
    int firstRightEnd(int A[], int n) {
        int l = 0, r = n - 2;
        while (l <= r) {
            int m = (l + r) / 2;
            if (A[m] > A[m + 1]) {
                return m;
            }
            else if (A[m] >= A[l]) {
                l = m + 1;
            }
            else {
                r = m - 1;
            }
        }
        return n - 1;
    }
    
    int binarySearch(int A[], int l, int r, int target) {
        while (l <= r) {
            int m = (l + r) / 2;
            if (A[m] < target) {
                l = m + 1;
            }
            else if (target < A[m]) {
                r = m - 1;
            }
            else {
                return m;
            }
        }
        return -1;
    }
};
