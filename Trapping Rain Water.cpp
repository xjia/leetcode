class Solution {
public:
    int trap(int A[], int n) {
        vector<int> levels(A, A + n);
        sort(levels.begin(), levels.end());
        
        int answer = 0, lastlevel = 0;
        for (const int level : levels) {
            int last = -1;
            for (int i = 0; i < n; i++) {
                if (A[i] >= level) {
                    if (last != -1) {
                        answer += (i - last - 1) * (level - lastlevel);
                    }
                    last = i;
                }
            }
            lastlevel = level;
        }
        return answer;
    }
};
