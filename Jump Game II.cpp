class Solution {
public:
    int jump(int A[], int n) {
        int farthest = 0;
        int jumps = 0;
        int last = 0;
        for (int i = 0; i < n; i++) {
            if (i > last) {
                jumps++;
                last = farthest;
            }
            farthest = max(farthest, i + A[i]);
        }
        return jumps;
    }
};
