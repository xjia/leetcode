public class Solution {
    /**
     * Find i,j such that numbers[i]+numbers[j]==target
     */
    public int[] twoSum(int[] numbers, int target) {
        Map<Integer, Integer> numberIndex = new HashMap<Integer, Integer>();
        
        for (int i = 0; i < numbers.length; i++) {
            numberIndex.put(numbers[i], i);
        }
        
        for (int i = 0; i < numbers.length; i++) {
            int theOther = target - numbers[i];
            if (numberIndex.containsKey(theOther)) {
                int index1 = i + 1;
                int index2 = numberIndex.get(theOther) + 1;
                if (index1 < index2)
                    return new int[]{ index1, index2 };
                else if (index1 > index2)
                    return new int[]{ index2, index1 };
            }
        }
        return null;
    }
}
