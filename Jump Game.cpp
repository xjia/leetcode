class Solution {
public:
    bool canJump(int A[], int n) {
        int farthest = 0;
        for (int i = 0; i < n; i++) {
            if (i > farthest) return false;
            farthest = max(farthest, i + A[i]);
        }
        return true;
    }
};
