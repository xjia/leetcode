class Solution {
    typedef unordered_map<string, int> hashtable;
    
public:
    vector<int> findSubstring(string S, vector<string> &L) {
        hashtable A;
        for (string word : L) increment(A, word);
        
        int n = L.size();
        int wordsize = L[0].size();
        
        vector<int> answer;
        for (int offset = 0; offset < wordsize; offset++)
            solve(offset, S, A, n, wordsize, answer);
        return answer;
    }
    
private:
    void solve(int offset, const string &S, hashtable &A,
               int n, int wordsize, vector<int> &answer) {
        hashtable B;
        int leftmost = offset;
        for (int i = offset; i < S.size(); i += wordsize) {
            string word = S.substr(i, wordsize);
            increment(B, word);
            while (count(B, word) > count(A, word) && leftmost <= i) {
                decrement(B, S.substr(leftmost, wordsize));
                leftmost += wordsize;
            }
            if (leftmost + wordsize * (n - 1) == i) {
                answer.push_back(leftmost);
            }
        }
    }
    
    void increment(hashtable &m, const string &x) {
        if (contains(m, x))
            m[x]++;
        else
            m[x] = 1;
    }
    
    void decrement(hashtable &m, const string &x) {
        if (contains(m, x))
            m[x]--;
    }
    
    int count(hashtable &m, const string &x) {
        if (contains(m, x))
            return m[x];
        return 0;
    }
    
    bool contains(hashtable &m, const string &x) {
        return m.find(x) != m.end();
    }
};
