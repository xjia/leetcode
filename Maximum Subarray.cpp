class Solution {
public:
    int maxSubArray(int A[], int n) {
        int largestSum = A[0];
        int sum = A[0];
        for (int i = 1; i < n; i++) {
            sum = max(sum + A[i], A[i]);
            largestSum = max(largestSum, sum);
        }
        return largestSum;
    }
};
