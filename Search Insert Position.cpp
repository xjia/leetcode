class Solution {
public:
    // find the first i such that A[i] >= target
    int searchInsert(int A[], int n, int target) {
        int l = 0, r = n - 1;
        while (l <= r) {
            int m = (l + r) / 2;
            if (A[m] == target) {
                return m;
            }
            else if (A[m] < target) {
                l = m + 1;
            }
            else {
                r = m - 1;
            }
        }
        return l;
    }
};
