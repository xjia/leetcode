class Solution {
public:
    int lengthOfLastWord(const char *s) {
        string str(s);
        int n = 0;
        auto rit = str.crbegin();
        while (rit != str.crend() && *rit == ' ') ++rit;
        while (rit != str.crend() && *rit != ' ') ++rit, ++n;
        return n;
    }
};
