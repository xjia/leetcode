/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        return add(l1, l2, 0);
    }
    
    private static ListNode add(ListNode a, ListNode b, int c) {
        if (a == null && b == null && c == 0) return null;
        if (a == null && b == null) return new ListNode(c);
        if (a == null) return add(b, c);
        if (b == null) return add(a, c);
        int sum = a.val + b.val + c;
        int digit = sum % 10;
        int carry = sum / 10;
        ListNode n = new ListNode(digit);
        n.next = add(a.next, b.next, carry);
        return n;
    }
    
    private static ListNode add(ListNode a, int c) {
        if (a == null && c == 0) return null;
        if (a == null) return new ListNode(c);
        if (c == 0) return a;
        int sum = a.val + c;
        int digit = sum % 10;
        int carry = sum / 10;
        ListNode n = new ListNode(digit);
        n.next = add(a.next, carry);
        return n;
    }
}
