class Solution {
public:
    vector<string> anagrams(vector<string> &strs) {
        unordered_multiset<string> anagrams;
        for (const string s : strs)
            anagrams.insert(key(s));
        vector<string> answer;
        for (const string s : strs)
            if (anagrams.count(key(s)) > 1)
                answer.push_back(s);
        return answer;
    }
    
private:
    string key(string s) {
        sort(s.begin(), s.end());
        return s;
    }
};
