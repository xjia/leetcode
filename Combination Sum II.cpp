class Solution {
public:
    vector<vector<int> > combinationSum2(vector<int> &candidates, int target) {
        typedef vector<int> combination;
        typedef vector<combination> combset;
        
        sort(candidates.begin(), candidates.end());
        
        vector<combset> f(target + 1);
        f[0].push_back(combination());
        
        for (const int ci : candidates)
            for (int j = target; j >= ci; j--)
                for (combination x : f[j - ci])
                    x.push_back(ci), f[j].push_back(x);
        
        combset answer = f[target];
        sort(answer.begin(), answer.end());
        auto it = unique(answer.begin(), answer.end());
        answer.resize(distance(answer.begin(), it));
        return answer;
    }
};
