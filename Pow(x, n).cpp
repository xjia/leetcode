class Solution {
public:
    double pow(double x, int n) {
        if (n == 0) return 1.0;
        if (n > 0) return fastexpt(x, n);
        return 1.0 / fastexpt(x, -n);
    }
    
private:
    double fastexpt(double x, int n) {
        double answer = 1.0;
        while (n) {
            if (n % 2) answer *= x;
            x *= x;
            n /= 2;
        }
        return answer;
    }
};
