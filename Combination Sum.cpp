class Solution {
public:
    vector<vector<int> > combinationSum(vector<int> &candidates, int target) {
        typedef vector<int> combination;
        typedef vector<combination> combset;
        
        sort(candidates.begin(), candidates.end());
        
        vector<combset> f(target + 1);
        f[0].push_back(combination());
        
        for (const int ci : candidates)
            for (int j = ci; j <= target; j++)
                for (combination x : f[j - ci])
                    x.push_back(ci), f[j].push_back(x);
        
        return f[target];
    }
};
