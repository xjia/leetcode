class Solution {
public:
    vector<vector<int> > permute(vector<int> &num) {
        int expected_size = 1;
        for (int i = 2; i <= num.size(); i++)
            expected_size *= i;
        vector<vector<int>> answer { num };
        while (answer.size() < expected_size) {
            next_permutation(num.begin(), num.end());
            answer.push_back(num);
        }
        return answer;
    }
};
