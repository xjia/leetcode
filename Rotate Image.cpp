class Solution {
public:
    void rotate(vector<vector<int> > &matrix) {
        const int n = matrix.size();
        const int w = (n + 1) / 2;
        const int h = n / 2;
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                swap(matrix[i][j], matrix[j][n - 1 - i]);
                swap(matrix[i][j], matrix[n - 1 - i][n - 1 - j]);
                swap(matrix[i][j], matrix[n - 1 - j][i]);
            }
        }
    }
};
