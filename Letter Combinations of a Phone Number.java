public class Solution {
    public ArrayList<String> letterCombinations(String digits) {
        ArrayList<String> combinations = new ArrayList<String>();
        combinations.add("");
        for (int i = 0; i < digits.length(); i++)
            combinations = expand(combinations, digits.charAt(i));
        return combinations;
    }
    
    private static final String[] mappings = {
        "", "", "abc", "def",
        "ghi", "jkl", "mno",
        "pqrs", "tuv", "wxyz"
    };
    
    private static ArrayList<String> expand(ArrayList<String> a, char c) {
        String mapping = mappings[c - '0'];
        ArrayList<String> b = new ArrayList<String>();
        for (int i = 0; i < mapping.length(); i++) {
            char suffix = mapping.charAt(i);
            for (String x : a) {
                b.add(x + suffix);
            }
        }
        return b;
    }
}
