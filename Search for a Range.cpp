class Solution {
public:
    vector<int> searchRange(int A[], int n, int target) {
        return vector<int> {
            searchLeft(A, 0, n - 1, target),
            searchRight(A, 0, n - 1, target)
        };
    }
    
private:
    int searchLeft(int A[], int l, int r, int target) {
        while (l < r) {
            int m = (l + r) / 2;
            if (A[m] < target) {
                l = m + 1;
            }
            else if (A[m] == target) {
                r = m;
            }
            else {
                r = m - 1;
            }
        }
        if (A[l] == target) return l;
        return -1;
    }
    
    int searchRight(int A[], int l, int r, int target) {
        while (l < r) {
            int m = (l + r + 1) / 2;
            if (A[m] < target) {
                l = m + 1;
            }
            else if (A[m] == target) {
                l = m;
            }
            else {
                r = m - 1;
            }
        }
        if (A[l] == target) return l;
        return -1;
    }
};
