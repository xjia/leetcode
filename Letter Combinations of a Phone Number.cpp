class Solution {
public:
    vector<string> letterCombinations(string digits) {
        vector<string> combinations { "" };
        for (char digit : digits)
            combinations = expand(combinations, digit);
        return combinations;
    }
    
private:
    static vector<string> expand(const vector<string> &a, char c) {
        static const vector<string> mappings {
            "", "", "abc", "def",
            "ghi", "jkl", "mno",
            "pqrs", "tuv", "wxyz"
        };
        
        vector<string> b;
        for (char suffix : mappings[c - '0'])
            for (string x : a)
                b.push_back(x + suffix);
        return b;
    }
};
