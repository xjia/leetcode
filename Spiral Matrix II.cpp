class Solution {
public:
    vector<vector<int> > generateMatrix(int n) {
        vector<vector<int>> matrix(n, vector<int>(n));
        vector<vector<bool>> visited(n, vector<bool>(n, false));
        const int D[4][2] = { {0,1}, {1,0}, {0,-1}, {-1,0} };
        int i = 0, j = 0, d = 0, c = 1;
        while (c <= n * n) {
            matrix[i][j] = c++;
            visited[i][j] = true;
            // try to move forward
            i += D[d][0];
            j += D[d][1];
            if (i < 0 || i >= n || j < 0 || j >= n || visited[i][j]) {
                i -= D[d][0];
                j -= D[d][1];
                d = (d + 1) % 4;
                i += D[d][0];
                j += D[d][1];
            }
        }
        return matrix;
    }
};
