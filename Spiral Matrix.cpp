class Solution {
public:
    vector<int> spiralOrder(vector<vector<int> > &matrix) {
        const int m = matrix.size();
        if (m == 0) return vector<int>();
        const int n = matrix[0].size();
        vector<vector<bool>> visited(m, vector<bool>(n, false));
        const int D[4][2] = { {0,1}, {1,0}, {0,-1}, {-1,0} };
        int i = 0, j = 0, d = 0, c = 0;
        vector<int> answer;
        while (c < m * n) {
            answer.push_back(matrix[i][j]);
            visited[i][j] = true;
            c++;
            // try to move forward
            i += D[d][0];
            j += D[d][1];
            if (i < 0 || i >= m || j < 0 || j >= n || visited[i][j]) {
                i -= D[d][0];
                j -= D[d][1];
                d = (d + 1) % 4;
                i += D[d][0];
                j += D[d][1];
            }
        }
        return answer;
    }
};
