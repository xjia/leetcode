class Solution {
public:
    vector<vector<int> > permuteUnique(vector<int> &num) {
        int expected_size = 1;
        for (int i = 2; i <= num.size(); i++)
            expected_size *= i;
        vector<vector<int>> answer { num };
        while (answer.size() < expected_size) {
            next_permutation(num.begin(), num.end());
            answer.push_back(num);
        }
        sort(answer.begin(), answer.end());
        auto it = unique(answer.begin(), answer.end());
        answer.resize(distance(answer.begin(), it));
        return answer;
    }
};
