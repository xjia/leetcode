class Solution {
public:
    void solveSudoku(vector<vector<char> > &board) {
        vector<vector<unordered_set<int>>> sudoku(9, vector<unordered_set<int>>(9));
        
        for (int i = 0; i < 9; i++)
            for (int j = 0; j < 9; j++)
                if (board[i][j] == '.')
                    sudoku[i][j] = unordered_set<int> { 1,2,3,4,5,6,7,8,9 };
                else
                    sudoku[i][j] = unordered_set<int> { board[i][j] - '0' };
        
        while (!isFinished(sudoku)) {
            bool updated = false;
            for (int i = 0; i < 9; i++)
                for (int j = 0; j < 9; j++)
                    if (sudoku[i][j].size() == 1) {
                        const int x = *(sudoku[i][j].cbegin());
                        board[i][j] = x + '0';
                        updated = true;
                        // update row
                        for (int k = 0; k < 9; k++) sudoku[i][k].erase(x);
                        // update column
                        for (int k = 0; k < 9; k++) sudoku[k][j].erase(x);
                        // update 3x3 grid
                        const int r0 = (i / 3) * 3;
                        const int c0 = (j / 3) * 3;
                        for (int r1 = 0; r1 < 3; r1++)
                            for (int c1 = 0; c1 < 3; c1++)
                                sudoku[r0 + r1][c0 + c1].erase(x);
                    }
            if (!updated) break;
        }
        
        search(0, board, sudoku);
    }
    
private:
    bool isFinished(const vector<vector<unordered_set<int>>> &sudoku) {
        for (int i = 0; i < 9; i++)
            for (int j = 0; j < 9; j++)
                if (sudoku[i][j].size() > 0)
                    return false;
        return true;
    }
    
    bool search(int cell, vector<vector<char> > &board, const vector<vector<unordered_set<int>>> &sudoku) {
        if (cell >= 81) return true;
        const int i = cell / 9;
        const int j = cell % 9;
        if (board[i][j] == '.') {
            for (int x : sudoku[i][j]) {
                board[i][j] = x + '0';
                if (isValidSudoku(board) && search(cell + 1, board, sudoku)) return true;
                board[i][j] = '.';
            }
            return false;
        }
        return search(cell + 1, board, sudoku);
    }
    
    bool isValidSudoku(vector<vector<char> > &board) {
        const int M = 3, N = M * M;
        
        // validate lines
        for (int i = 0; i < N; i++) {
            vector<bool> used(256, false);
            for (int j = 0; j < N; j++)
                if (board[i][j] != '.' && used[board[i][j]])
                    return false;
                else
                    used[board[i][j]] = true;
        }
        
        // validate columns
        for (int j = 0; j < N; j++) {
            vector<bool> used(256, false);
            for (int i = 0; i < N; i++)
                if (board[i][j] != '.' && used[board[i][j]])
                    return false;
                else
                    used[board[i][j]] = true;
        }
        
        // validate 3x3 grids
        for (int grid = 0; grid < N; grid++) {
            const int r = (grid / M) * M;
            const int c = (grid % M) * M;
            vector<bool> used(256, false);
            for (int i = 0; i < M; i++)
                for (int j = 0; j < M; j++)
                    if (board[r + i][c + j] != '.' && used[board[r + i][c + j]])
                        return false;
                    else
                        used[board[r + i][c + j]] = true;
        }
        
        return true;
    }
};
