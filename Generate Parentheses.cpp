class Solution {
public:
    vector<string> generateParenthesis(int n) {
        vector<unordered_set<string>> levels(n + 1, unordered_set<string>{});
        levels[0].insert("");
        for (int k = 1; k <= n; k++) {
            for (string s : levels[k - 1]) {
                levels[k].insert("(" + s + ")");
            }
            for (int i = 1; i < k; i++) {
                for (string s : levels[i]) {
                    for (string t : levels[k - i]) {
                        levels[k].insert(s + t);
                    }
                }
            }
        }
        
        vector<string> answer;
        for (string s : levels[n]) {
            answer.push_back(s);
        }
        return answer;
    }
};
