class Solution {
public:
    string countAndSay(int n) {
        string s = "1";
        for (int i = 2; i <= n; i++)
            s = next(s);
        return s;
    }

private:
    string next(const string &s) {
        string t = "";
        int i = 0;
        while (i < s.size()) {
            int j = i + 1;
            while (j < s.size() && s[j] == s[i])
                ++j;
            const int count = j - i;
            t += toString(count) + s[i];
            i = j;
        }
        return t;
    }
    
    string toString(int n) {
        ostringstream oss;
        oss << n;
        return oss.str();
    }
};
