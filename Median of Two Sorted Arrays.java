public class Solution {
    /**
     * There are two sorted arrays A and B of size m and n respectively.
     * Find the median of the two sorted arrays.
     * The overall run time complexity should be O(log (m+n)).
     */
    public double findMedianSortedArrays(int A[], int B[]) {
        final int m = A.length, n = B.length;
        final int abSum = (m + n) / 2;
        int l = 0, r = Math.min(abSum, m);
        while (l <= r) {
            final int a = (l + r) / 2;
            final int b = abSum - a;
            final int A1 = nth(A, a);
            final int A2 = nth(A, a + 1);
            final int B1 = nth(B, b);
            final int B2 = nth(B, b + 1);
            
            if (A1 <= B2 && B1 <= A2) {
                if ((m + n) % 2 == 0) {
                    return (Math.max(A1, B1) + Math.min(A2, B2)) / 2.0;
                }
                else {
                    return Math.min(A2, B2);
                }
            }
            else if (A1 <= B2 && A2 <= B1) {
                l = a + 1;
            }
            else if (B1 <= A2 && B2 <= A1) {
                r = a - 1;
            }
            else {
                throw new RuntimeException("impossible branch");
            }
        }
        throw new RuntimeException("cannot reach here");
    }
    
    private static int nth(int X[], int n) {
        if (n <= 0) return Integer.MIN_VALUE;
        if (n > X.length) return Integer.MAX_VALUE;
        return X[n - 1];
    }
}
