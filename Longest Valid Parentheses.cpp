class Solution {
public:
    /**
     * Given a string containing just the characters '(' and ')',
     * find the length of the longest valid (well-formed) parentheses substring.
     * 
     * Examples:
     * 
     * "(()"        => 2
     * ")()())"     => 4
     * "()(()"      => 2
     * "((()()))"   => 8
     * "()()(()())" => 10
     * 
     * The variable R below has two meanings:
     * (1) the length of the longest valid parentheses substring
     *     discovered so far since the last unmatched left '('
     * (2) (when pushed onto the stack) the length of previous
     *     matched consecutive valid parentheses substring, which
     *     should be added to the length of the longest on discovered
     *     so far; consider "()()()" => 6, the sequence of operations
     *     is: push(0) for '(', update(2) for ')', push(2) for '(',
     *     update(4) for ')', push(4) for '(', update(6) for ')'.
     */
    int longestValidParentheses(string s) {
        stack<int> t;
        int answer = 0;
        int R = 0;
        for (char c : s) {
            if (c == '(') {
                t.push(R);
                R = 0;
            }
            else {
                if (t.empty()) {
                    R = 0;
                }
                else {
                    R = t.top() + R + 2;
                    answer = max(answer, R);
                    t.pop();
                }
            }
        }
        return answer;
    }
    /**
     * This problem can be solved by using dynamic programming techniques.
     * Let f(i) denote the length of the longest valid parentheses substring
     * that exactly ends with s[i].  Then it is easy to solve this problem.
     */
};
