class Solution {
public:
    vector<vector<string> > solveNQueens(int n) {
        vector<vector<string>> solutions;
        vector<int> q(n);
        vector<bool> c(n, false);
        vector<bool> d1(n + n - 1, false);
        vector<bool> d2(n + n - 1, false);
        search(0, n, q, c, d1, d2, solutions);
        return solutions;
    }
    
private:
    void search(int i, const int n, vector<int> &q, vector<bool> &c, vector<bool> &d1, vector<bool> &d2, vector<vector<string>> &solutions) {
        if (i == n) {
            vector<string> solution;
            for (int r = 0; r < n; r++) {
                string line = "";
                for (int c = 0; c < q[r]; c++) line += '.';
                line += 'Q';
                for (int c = q[r] + 1; c < n; c++) line += '.';
                solution.push_back(line);
            }
            solutions.push_back(solution);
        }
        else {
            for (int j = 0; j < n; j++) {
                if (c[j] || d1[i - j + n - 1] || d2[i + j]) continue;
                q[i] = j;
                c[j] = d1[i - j + n - 1] = d2[i + j] = true;
                search(i + 1, n, q, c, d1, d2, solutions);
                c[j] = d1[i - j + n - 1] = d2[i + j] = false;
            }
        }
    }
};
