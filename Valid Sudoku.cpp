class Solution {
public:
    bool isValidSudoku(vector<vector<char> > &board) {
        const int M = 3, N = M * M;
        if (board.size() != N)
            return false;
        for (int i = 0; i < N; i++)
            if (board[i].size() != N)
                return false;
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                if (!(board[i][j] == '.' || ('1' <= board[i][j] && board[i][j] <= '9')))
                    return false;
        
        // validate lines
        for (int i = 0; i < N; i++) {
            vector<bool> used(256, false);
            for (int j = 0; j < N; j++)
                if (board[i][j] != '.' && used[board[i][j]])
                    return false;
                else
                    used[board[i][j]] = true;
        }
        
        // validate columns
        for (int j = 0; j < N; j++) {
            vector<bool> used(256, false);
            for (int i = 0; i < N; i++)
                if (board[i][j] != '.' && used[board[i][j]])
                    return false;
                else
                    used[board[i][j]] = true;
        }
        
        // validate 3x3 grids
        for (int grid = 0; grid < N; grid++) {
            int r = grid / M;
            int c = grid % M;
            r *= M;
            c *= M;
            vector<bool> used(256, false);
            for (int i = 0; i < M; i++)
                for (int j = 0; j < M; j++)
                    if (board[r + i][c + j] != '.' && used[board[r + i][c + j]])
                        return false;
                    else
                        used[board[r + i][c + j]] = true;
        }
        
        return true;
    }
};
