/**
 * Definition for an interval.
 * struct Interval {
 *     int start;
 *     int end;
 *     Interval() : start(0), end(0) {}
 *     Interval(int s, int e) : start(s), end(e) {}
 * };
 */
class Solution {
public:
    vector<Interval> merge(vector<Interval> &intervals) {
        sort(intervals.begin(), intervals.end(), compare);
        vector<Interval> answer;
        int i = 0;
        while (i < intervals.size()) {
            int leftmost = intervals[i].start;
            int rightmost = intervals[i].end;
            int j = i + 1;
            while (j < intervals.size() && rightmost >= intervals[j].start) {
                rightmost = max(rightmost, intervals[j].end);
                j++;
            }
            answer.push_back(Interval(leftmost, rightmost));
            i = j;
        }
        return answer;
    }
    
private:
    static bool compare(const Interval &a, const Interval &b) {
        return make_pair(a.start, a.end) < make_pair(b.start, b.end);
    }
};
