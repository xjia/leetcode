class Solution {
public:
    string multiply(string num1, string num2) {
        reverse(num1.begin(), num1.end());
        reverse(num2.begin(), num2.end());
        
        vector<int> n(num1.size() * num2.size() + 1, 0);
        for (int i = 0; i < num1.size(); i++)
            for (int j = 0; j < num2.size(); j++)
                n[i + j] += (num1[i] - '0') * (num2[j] - '0');
        
        int carry = 0;
        for (int i = 0; i < n.size(); i++) {
            n[i] += carry;
            carry = n[i] / 10;
            n[i] %= 10;
        }
        
        int nonzero = n.size() - 1;
        while (nonzero > 0 && n[nonzero] == 0)
            nonzero--;
        
        string answer;
        for (int i = nonzero; i >= 0; i--)
            answer += n[i] + '0';
        return answer;
    }
};
